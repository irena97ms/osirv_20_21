# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 11:55:28 2020

@author: irena
"""

import numpy
import cv2

#**************PRVI*************

# slika = cv2.imread('D:\osirv_labs\osirv_20_21\lab1\slike\lenna.bmp')

# crvena=slika.copy()
# crvena[:,:,(0,1)]*=0

# plava=slika.copy()
# plava[:,:,(1,2)]*=0

# zelena=slika.copy()
# zelena[:,:,(0,2)]*=0

# cv2.imwrite('D:\osirv_labs\osirv_20_21\lab1\slike\crvena.jpg',crvena)
# cv2.imwrite('D:\osirv_labs\osirv_20_21\lab1\slike\plava.jpg',plava)
# cv2.imwrite('D:\osirv_labs\osirv_20_21\lab1\slike\zelena.jpg',zelena)



#**************DRUGI*************

# slika = cv2.imread('D:\osirv_labs\osirv_20_21\lab1\slike\lenna.bmp') 
 
# plava, zelena, crvena = cv2.split(slika)
  
# zeros = numpy.zeros(plava.shape, numpy.uint8)
 
# blueBGR = cv2.merge((plava,zeros,zeros))
# greenBGR = cv2.merge((zeros,zelena,zeros))
# redBGR = cv2.merge((zeros,zeros,crvena))
 
 
# cv2.imshow('plava.jpg', blueBGR)
# cv2.imshow('zelena.jpg', greenBGR)
# cv2.imshow('crvena.jpg', redBGR)
 
# cv2.waitKey(0)
# cv2.destroyAllWindows()



#**************TRECI*************

# slika = cv2.imread('D:\osirv_labs\osirv_20_21\lab1\slike\lenna.bmp')

# top, bottom, left, right = [45]*4
# obrub = cv2.copyMakeBorder(slika, top, bottom, left, right, cv2.BORDER_CONSTANT)
# cv2.imshow('imageborder',obrub)

# cv2.waitKey()
# cv2.destroyAllWindows()



#**************cetvrti*************

slika = cv2.imread('D:\osirv_labs\osirv_20_21\lab1\slike\lenna.bmp')

cv2.imwrite("cetvrti_zad.jpg", slika)
cv2.imwrite("sl.jpg", slika[:100,:100,:])

img_1=slika.copy()[range(0,slika.shape[0],2),:,:]
print(img_1.shape)
img_2=slika.copy()[:,range(0,slika.shape[1],2),:]
print(img_2.shape)
img_3=slika.copy()[0:(slika.shape[0]):2, 0:(slika.shape[1]):2, :]
print(img_3.shape)

cv2.imshow('sl1',img_1)
cv2.imshow('sl2',img_2)
cv2.imshow('sl3',img_3)

cv2.waitKey()
cv2.destroyAllWindows()
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 16:02:16 2020

@author: irena
"""

import numpy as np
import cv2

slika1 = cv2.imread("D:/osirv_labs/osirv_20_21/lab2/slike/lenna.bmp")
slika2 = cv2.imread("D:/osirv_labs/osirv_20_21/lab2/slike/airplane.bmp")
slika3 = cv2.imread("D:/osirv_labs/osirv_20_21/lab2/slike/pepper.bmp")
novaSlika = np.vstack((slika1,slika2,slika3))
top, bottom, left, right = [50]*4
cv2.imwrite("D:/osirv_labs/osirv_20_21/lab2/slike/novaSlika.bmp", novaSlika)
obrubSlika = cv2.copyMakeBorder(novaSlika, top, bottom, left, right, cv2.BORDER_CONSTANT)
cv2.imshow("Slika s obrubom",obrubSlika)
cv2.waitKey(0)
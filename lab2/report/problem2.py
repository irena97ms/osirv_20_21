# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 17:27:30 2020

@author: irena
"""

import matplotlib.pyplot as plt
import numpy as np
import cv2

img = cv2.imread("D:/osirv_labs/osirv_20_21/lab2/slike/baboon.bmp")
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

kernel = np.array (([-1, 1, 0],
                    [4, -4, 1],
                    [0, 1, 0]), np.float32)

print(kernel)

output = cv2.filter2D(img, -1, kernel)

plt.subplot(1, 2, 1)
plt.imshow(img)
plt.title("Originalna slika")

plt.subplot(1, 2, 2)
plt.imshow(output)
plt.title("Konvolucija")

plt.show()
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 17:59:38 2020

@author: irena
"""

import matplotlib.pyplot as plt
import cv2

img1 = cv2.imread("D:/osirv_labs/osirv_20_21/lab2/slike/baboon.bmp")
img2 = cv2.imread("D:/osirv_labs/osirv_20_21/lab2/slike/lenna.bmp")
img3 = cv2.imread("D:/osirv_labs/osirv_20_21/lab2/slike/barbara.bmp")

img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
img3 = cv2.cvtColor(img3, cv2.COLOR_BGR2RGB)

output1 = cv2.bitwise_not(img1)
output2 = cv2.bitwise_not(img2)
output3 = cv2.bitwise_not(img3)

cv2.imwrite("D:/osirv_labs/osirv_20_21/lab2/slike/problem3/baboon_invert.png", output1)
cv2.imwrite("D:/osirv_labs/osirv_20_21/lab2/slike/problem3/lenna_invert.png", output2)
cv2.imwrite("D:/osirv_labs/osirv_20_21/lab2/slike/problem3/barbara_invert.png", output3)

plt.subplot(1, 2, 1)
plt.imshow(img1)
plt.title("Originalna slika")

plt.subplot(1, 2, 2)
plt.imshow(output1)
plt.title("Invert")

plt.show()

plt.subplot(2, 2, 3)
plt.imshow(img2)
plt.title("Originalna slika")

plt.subplot(2, 2, 4)
plt.imshow(output2)
plt.title("Invert")

plt.show()

plt.subplot(3, 2, 5)
plt.imshow(img3)
plt.title("Originalna slika")

plt.subplot(3, 2, 6)
plt.imshow(output3)
plt.title("Invert")

plt.show()
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 18:43:47 2020

@author: irena
"""

import matplotlib.pyplot as plt
import cv2

img1 = cv2.imread("D:/osirv_labs/osirv_20_21/lab2/slike/baboon.bmp", 0)
img2 = cv2.imread("D:/osirv_labs/osirv_20_21/lab2/slike/lenna.bmp")
img3 = cv2.imread("D:/osirv_labs/osirv_20_21/lab2/slike/barbara.bmp")

img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
img3 = cv2.cvtColor(img3, cv2.COLOR_BGR2RGB)

#baboon

ret, baboon_th1 = cv2.threshold(img1, 63, 255, cv2.THRESH_BINARY)
plt.imshow(baboon_th1)
plt.title("Threshold_63")
plt.show()

ret, baboon_th2 = cv2.threshold(img1, 127, 255, cv2.THRESH_BINARY)
plt.imshow(baboon_th2)
plt.title("Threshold_127")
plt.show()

ret, baboon_th3 = cv2.threshold(img1, 191, 255, cv2.THRESH_BINARY)
plt.imshow(baboon_th3)
plt.title("Threshold_191")
plt.show()

#lenna

ret, lenna_th1 = cv2.threshold(img2, 63, 255, cv2.THRESH_BINARY)
plt.imshow(lenna_th1)
plt.title("Threshold_63")
plt.show()

ret, lenna_th2 = cv2.threshold(img2, 127, 255, cv2.THRESH_BINARY)
plt.imshow(lenna_th2)
plt.title("Threshold_127")
plt.show()

ret, lenna_th3 = cv2.threshold(img2, 191, 255, cv2.THRESH_BINARY)
plt.imshow(lenna_th3)
plt.title("Threshold_191")
plt.show()

#barbara

ret, barbara_th1 = cv2.threshold(img3, 63, 255, cv2.THRESH_BINARY)
plt.imshow(barbara_th1)
plt.title("Threshold_63")
plt.show()

ret, barbara_th2 = cv2.threshold(img3, 127, 255, cv2.THRESH_BINARY)
plt.imshow(barbara_th2)
plt.title("Threshold_127")
plt.show()

ret, barbara_th3 = cv2.threshold(img3, 191, 255, cv2.THRESH_BINARY)
plt.imshow(barbara_th3)
plt.title("Threshold_191")
plt.show()

cv2.imwrite("D:/osirv_labs/osirv_20_21/lab2/slike/problem4/baboon_63_thresh.png", baboon_th1)
cv2.imwrite("D:/osirv_labs/osirv_20_21/lab2/slike/problem4/baboon_127_thresh.png", baboon_th2)
cv2.imwrite("D:/osirv_labs/osirv_20_21/lab2/slike/problem4/baboon_191_thresh.png", baboon_th3)

cv2.imwrite("D:/osirv_labs/osirv_20_21/lab2/slike/problem4/lenna_63_thresh.png", lenna_th1)
cv2.imwrite("D:/osirv_labs/osirv_20_21/lab2/slike/problem4/lenna_127_thresh.png", lenna_th2)
cv2.imwrite("D:/osirv_labs/osirv_20_21/lab2/slike/problem4/lenna_191_thresh.png", lenna_th3)

cv2.imwrite("D:/osirv_labs/osirv_20_21/lab2/slike/problem4/barbara_63_thresh.png", barbara_th1)
cv2.imwrite("D:/osirv_labs/osirv_20_21/lab2/slike/problem4/barbara_127_thresh.png", barbara_th2)
cv2.imwrite("D:/osirv_labs/osirv_20_21/lab2/slike/problem4/barbara_191_thresh.png", barbara_th3)
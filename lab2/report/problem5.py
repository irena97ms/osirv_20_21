# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 12:12:46 2020

@author: irena
"""

import numpy as np
import cv2


def kvantizacija(q):
    img=cv2.imread("D:/osirv_labs/osirv_20_21/lab2/slike/BoatsColor.bmp", 0)
    img=img.astype(np.float32)
    d=pow(2, 8-q)
    height=np.size(img,0)
    width=np.size(img,1)
    for i in range(height):
        for j in range(width):
            img[i,j]=(np.floor(img[i,j]/d)+0.5)*d
            if (img[i,j]>255):
                img[i,j]=255
            elif(img[i,j]<0):
                img[i,j]=0

    maxx=np.max(img)
    minn=np.min(img)
    uniquee=np.unique(img)
    print("Max value: ", maxx)
    print("Min value: ", minn,)
    print("Unique value: ", uniquee)
    img=img.astype(np.uint8)
    path="D:/osirv_labs/osirv_20_21/lab2/slike/problem5/Boat"+str(q)+".bmp"
    cv2.imwrite(path, img)
                
br=8
while(br>0):
    kvantizacija(br)
    br=br-1
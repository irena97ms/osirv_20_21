# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 12:36:34 2020

@author: irena
"""

import cv2
import numpy as np

img = cv2.imread("D:/osirv_labs/osirv_20_21/lab2/slike/baboon.bmp", 0)
img = cv2.resize(img, None, fx=0.25, fy=0.25, interpolation = cv2.INTER_CUBIC)
rows, cols = img.shape

images = []

for degree in range (0, 360, 30):
    M = cv2.getRotationMatrix2D((cols/2, rows/2), degree, 1)
    dst = cv2.warpAffine(img, M, (cols, rows))
    images.append(dst)


rotated_img = np.hstack(images)
cv2.imshow("baboon_rotated", rotated_img)
cv2.waitKey(0)
cv2.destroyAllWindows

cv2.imwrite("D:/osirv_labs/osirv_20_21/lab2/slike/baboon_rotated.bmp", rotated_img)

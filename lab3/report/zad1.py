# -*- coding: utf-8 -*-
"""
Created on Mon Dec 14 16:59:41 2020

@author: irena
"""

import numpy as np
import cv2
import matplotlib.pyplot as plt
import os



def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)


def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()
def showhist(img):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.show()

img = cv2.imread("D:/osirv_labs/osirv_20_21/lab3/slike/lenna.bmp",0)
show(img)
showhist(img)


#gaussian_noise
show(gaussian_noise(img, 0, 20))
showhist(gaussian_noise(img, 0, 20))
show(gaussian_noise(img, 0, 30))
showhist(gaussian_noise(img, 0, 30))
show(gaussian_noise(img, 0, 40))
showhist(gaussian_noise(img, 0, 40))

#uniform_noise
show(uniform_noise(img, -20, 20))
showhist(uniform_noise(img, -20, 20))
show(uniform_noise(img, -40, 40))
showhist(uniform_noise(img, -40, 40))
show(uniform_noise(img, -60, 60))
showhist(uniform_noise(img, -60, 60))

#salt_n_pepper_noise
show(salt_n_pepper_noise(img, 5))
showhist(salt_n_pepper_noise(img, 5))
show(salt_n_pepper_noise(img, 10))
showhist(salt_n_pepper_noise(img, 10))
show(salt_n_pepper_noise(img, 15))
showhist(salt_n_pepper_noise(img, 15))
show(salt_n_pepper_noise(img, 20))
showhist(salt_n_pepper_noise(img, 20))

directory="D:/osirv_labs/osirv_20_21/lab3/slike/zad1"
os.chdir(directory) 
cv2.imwrite("lenna_gaussian_noise_20.bmp", gaussian_noise(img, 0, 20))
cv2.imwrite("lenna_gaussian_noise_30.bmp", gaussian_noise(img, 0, 30))
cv2.imwrite("lenna_gaussian_noise_40.bmp", gaussian_noise(img, 0, 40))
cv2.imwrite("lenna_uniform_noise_20.bmp", uniform_noise(img, -20, 20))
cv2.imwrite("lenna_uniform_noise_40.bmp", uniform_noise(img, -40, 40))
cv2.imwrite("lenna_uniform_noise_60.bmp", uniform_noise(img, -60, 60))
cv2.imwrite("lenna_salt_n_pepper_noise_5.bmp", salt_n_pepper_noise(img, 5))
cv2.imwrite("lenna_salt_n_pepper_noise_10.bmp", salt_n_pepper_noise(img, 10))
cv2.imwrite("lenna_salt_n_pepper_noise_55.bmp", salt_n_pepper_noise(img, 15))
cv2.imwrite("lenna_salt_n_pepper_noise_20.bmp", salt_n_pepper_noise(img, 20))

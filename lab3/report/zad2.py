# -*- coding: utf-8 -*-
"""
Created on Mon Dec 14 18:06:20 2020

@author: irena
"""

import numpy as np
import cv2
import matplotlib.pyplot as plt
import os



def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)


def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()
def showhist(img):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.show()

img1 = cv2.imread("D:/osirv_labs/osirv_20_21/lab3/slike/airplane.bmp",0)
img2 = cv2.imread("D:/osirv_labs/osirv_20_21/lab3/slike/boats.bmp",0)

#airplane
gaussian_airplane5=(gaussian_noise(img1, 0, 5))
gaussian_airplane15=(gaussian_noise(img1, 0, 15))
gaussian_airplane35=(gaussian_noise(img1, 0, 35))
salt_n_pepper_airplane1=(salt_n_pepper_noise(img1, 1));
salt_n_pepper_airplane10=(salt_n_pepper_noise(img1, 10));

#boats
gaussian_boats5=(gaussian_noise(img2, 0, 5))
gaussian_boats15=(gaussian_noise(img2, 0, 15))
gaussian_boats35=(gaussian_noise(img2, 0, 35))
salt_n_pepper_boats1=(salt_n_pepper_noise(img2, 1));
salt_n_pepper_boats10=(salt_n_pepper_noise(img2, 10));

saltpep1radius3=cv2.medianBlur(salt_n_pepper_boats1,3)
saltpep1radius5=cv2.medianBlur(salt_n_pepper_boats1,5)
saltpep1radius7=cv2.medianBlur(salt_n_pepper_boats1,7)


images = [gaussian_airplane5, gaussian_airplane15, gaussian_airplane35, salt_n_pepper_airplane1, salt_n_pepper_airplane10,
          gaussian_boats5, gaussian_boats15, gaussian_boats35, salt_n_pepper_boats1, salt_n_pepper_boats10]

med_img_15=[]
gauss_img_25=[]

for i in range(len(images)):
    med_img_15.append(cv2.medianBlur(images[i],15))

for i in range(len(images)):
    gauss_img_25.append(cv2.GaussianBlur(images[i],(25, 25),25))

for i in range(len(gauss_img_25)):
    cv2.imshow("gauss", gauss_img_25[i])
    cv2.waitKey(0)
    cv2.destroyAllWindows()


for i in range(len(med_img_15)):
    cv2.imshow("med", med_img_15[i])
    cv2.waitKey(0)
    cv2.destroyAllWindows()

for i in range(len(gauss_img_25)):
    cv2.imwrite("D:/osirv_labs/osirv_20_21/lab3/slike/zad2/gauss"+str(i)+".bmp",gauss_img_25[i])
    cv2.imwrite("D:/osirv_labs/osirv_20_21/lab3/slike/zad2/med"+str(i)+".bmp",med_img_15[i])
    
med_img_5=[]
gauss_img_11=[]

for i in range(len(images)):
    med_img_5.append(cv2.medianBlur(images[i],5))

for i in range(len(images)):
    gauss_img_11.append(cv2.GaussianBlur(images[i],(11, 11),11))

for i in range(len(gauss_img_11)):
    cv2.imshow("gauss", gauss_img_11[i])
    cv2.waitKey(0)
    cv2.destroyAllWindows()


for i in range(len(med_img_5)):
    cv2.imshow("med", med_img_5[i])
    cv2.waitKey(0)
    cv2.destroyAllWindows()

for i in range(len(gauss_img_11)):
    cv2.imwrite("D:/osirv_labs/osirv_20_21/lab3/slike/zad2/gauss2_"+str(i)+".bmp",gauss_img_11[i])
    cv2.imwrite("D:/osirv_labs/osirv_20_21/lab3/slike/zad2/med2_"+str(i)+".bmp",med_img_5[i])
    
    
#mymedian

def my_med(img1, radius):
    imagecopy=img1
    for i in range(img1.shape[0]):
        for j in range(img1.shape[1]):
            imagecopy[i][j]=np.median(imagecopy[i:i+radius, j:j+radius])
    return imagecopy

m=my_med(img1, 5)
directory="D:/osirv_labs/osirv_20_21/lab3/slike/zad2"
os.chdir(directory) 
cv2.imwrite("my median.jpg", m)

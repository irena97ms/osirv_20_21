# -*- coding: utf-8 -*-
"""
Created on Mon Dec 14 19:01:33 2020

@author: irena
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

img_boats = cv2.imread("D:/osirv_labs/osirv_20_21/lab3/slike/boats.bmp",0)
img_airplane = cv2.imread("D:/osirv_labs/osirv_20_21/lab3/slike/airplane.bmp",0)
img_baboon = cv2.imread("D:/osirv_labs/osirv_20_21/lab3/slike/baboon.bmp",0)
img_boats = gaussian_noise(img_boats, 0, 30)

#boats
ret,th1_boats = cv2.threshold(img_boats,127,255,cv2.THRESH_BINARY)
ret,th2_boats = cv2.threshold(img_boats,127,255,cv2.THRESH_BINARY_INV)
ret,th3_boats = cv2.threshold(img_boats,127,255,cv2.THRESH_TRUNC)
ret,th4_boats = cv2.threshold(img_boats,127,255,cv2.THRESH_TRIANGLE)
ret,th5_boats = cv2.threshold(img_boats,127,255,cv2.THRESH_MASK)
ret,th6_boats = cv2.threshold(img_boats,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
# Otsu's thresholding after Gaussian filtering
blur = cv2.GaussianBlur(img_boats,(5,5),0)
ret,th7_boats = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

boats=[th1_boats,th2_boats,th3_boats,th4_boats,th5_boats,th6_boats,th7_boats]
for i in range(len(boats)):
    cv2.imwrite("D:/osirv_labs/osirv_20_21/lab3/slike/zad3/boats_th"+str(i)+".bmp", boats[i])


#airplane
ret,th1_airplane = cv2.threshold(img_airplane,127,255,cv2.THRESH_BINARY)
ret,th2_airplane = cv2.threshold(img_airplane,127,255,cv2.THRESH_BINARY_INV)
ret,th3_airplane = cv2.threshold(img_airplane,127,255,cv2.THRESH_TRUNC)
ret,th4_airplane = cv2.threshold(img_airplane,127,255,cv2.THRESH_TRIANGLE)
ret,th5_airplane = cv2.threshold(img_airplane,127,255,cv2.THRESH_MASK)
ret,th6_airplane = cv2.threshold(img_airplane,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
# Otsu's thresholding after Gaussian filtering
blur = cv2.GaussianBlur(img_airplane,(5,5),0)
ret,th7_airplane = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

airplanes=[th1_airplane,th2_airplane,th3_airplane,th4_airplane,th5_airplane,th6_airplane,th7_airplane]
for i in range(len(airplanes)):
    cv2.imwrite("D:/osirv_labs/osirv_20_21/lab3/slike/zad3/airplanes_th"+str(i)+".bmp", airplanes[i])


#baboon
ret,th1_baboon = cv2.threshold(img_baboon,127,255,cv2.THRESH_BINARY)
ret,th2_baboon = cv2.threshold(img_baboon,127,255,cv2.THRESH_BINARY_INV)
ret,th3_baboon = cv2.threshold(img_baboon,127,255,cv2.THRESH_TRUNC)
ret,th4_baboon = cv2.threshold(img_baboon,127,255,cv2.THRESH_TRIANGLE)
ret,th5_baboon = cv2.threshold(img_baboon,127,255,cv2.THRESH_MASK)
ret,th6_baboon = cv2.threshold(img_baboon,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
# Otsu's thresholding after Gaussian filtering
blur = cv2.GaussianBlur(img_baboon,(5,5),0)
ret,th7_baboon = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

baboons=[th1_baboon,th2_baboon,th3_baboon,th4_baboon,th5_baboon,th6_baboon,th7_baboon,]
for i in range(len(baboons)):
    cv2.imwrite("D:/osirv_labs/osirv_20_21/lab3/slike/zad3/baboons_th"+str(i)+".bmp", baboons[i])
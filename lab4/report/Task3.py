# -*- coding: utf-8 -*-
"""
Created on Wed Dec 23 16:51:26 2020

@author: irena
"""

import cv2
import numpy as np
import math
import copy
from matplotlib import pyplot as plt
import os

img = cv2.imread('D:/osirv_labs/lab4/slike/chess.jpg')
img2 = copy.copy(img)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(gray, 166, 255)

lines= cv2.HoughLines(edges, 1, math.pi/180, 150, np.array([]), 0, 0)

a, b, c = lines.shape
for i in range(a):
    rho = lines[i][0][0]
    theta = lines[i][0][1]
    a = math.cos(theta)
    b = math.sin(theta)
    x0, y0 = a*rho, b*rho
    pt1 = ( int(x0+1000*(-b)), int(y0+1000*(a)) )
    pt2 = ( int(x0-1000*(-b)), int(y0-1000*(a)) )
    cv2.line(img2, pt1, pt2, (255, 0, 0), 2, cv2.LINE_AA)

plt.subplot(121),plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(img2, 'gray')
plt.title('Detected Lines'), plt.xticks([]), plt.yticks([])
plt.show()


path = r'D:/osirv_labs/lab4/report/slike'
os.chdir(path)

#cv2.imwrite("chessboard_90-150.jpg", img2)  #najbolji rezultat
#cv2.imwrite("chessboard_180-200.jpg", img2)
#cv2.imwrite("chessboard_90-200.jpg", img2)
cv2.imwrite("chessboard_180-150.jpg", img2)
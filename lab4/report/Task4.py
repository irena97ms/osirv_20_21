# -*- coding: utf-8 -*-
"""
Created on Wed Dec 23 22:01:12 2020

@author: irena
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt
import copy
import os

img = cv2.imread('D:/osirv_labs/lab4/slike/chess.jpg')
img2 = copy.copy(img)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)
#dst = cv2.cornerHarris(gray,2,3,0.04)
#dst = cv2.cornerHarris(gray,1,3,0.04)
#dst = cv2.cornerHarris(gray,3,3,0.04)
dst = cv2.cornerHarris(gray,5,3,0.04)

#result is dilated for marking the corners, not important
dst = cv2.dilate(dst,None)

# threshold for an optimal value, it may vary depending on the image.
img2[dst>0.01*dst.max()]=[0,0,255]

plt.subplot(121),plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(img2 , 'gray')
plt.title('Detected Corners'), plt.xticks([]), plt.yticks([])
plt.show()

path = r'D:/osirv_labs/lab4/report/slike'
os.chdir(path)

#cv2.imwrite("chess_1.jpg", img2)  
#cv2.imwrite("chess_3.jpg", img2)
cv2.imwrite("chess_5.jpg", img2)

TASK 1

import numpy as np
import cv2


cap = cv2.VideoCapture('D:/osirv_labs/lab5/road.mp4')

# define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi',fourcc, 20.0, (640,480))

while(cap.isOpened()):
    ret, frame = cap.read()
    if ret==True:
        frame = cv2.flip(frame, -180)

        # write the flipped frame
        out.write(frame)

        # Display the resulting frame
        cv2.imshow('Frame',frame)
        k = cv2.waitKey(30) & 0xff
        if k == 27:
             break
# release everything if job is finished
cap.release()
out.release()
cv2.destroyAllWindows()


TASK 2

Saprse optical flow - need to process some pixels from the whole image
Dense optical flow - process all the pixels, slower but can be more accurate


TASK 3

import cv2 

vid = cv2.VideoCapture(0) 
  
while(True): 
    ret, frame = vid.read() 
    cv2.imshow('frame', frame) 
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break
  
vid.release() 
cv2.destroyAllWindows() 